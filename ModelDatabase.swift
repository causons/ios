//
//  ModelDatabase.swift
//  Causons
//
//  Created by Thibault Farnier on 03/06/2015.
//  Copyright (c) 2015 Causons. All rights reserved.
//

import UIKit
import CoreData

class ModelDatabase: NSObject {

    // MARK: Context
    
    static func getContext() -> NSManagedObjectContext {
        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        return delegate.managedObjectContext!
    }
    
    // MARK: Generic Functions
    
    static func getDataArrayWithPredicate(predicate:NSPredicate?, inTable table:String, withSortDescriptors sortDescriptors:[AnyObject]?) -> [NSManagedObject] {
        let context = ModelDatabase.getContext()
        let fetchRequest = NSFetchRequest()
        let entity = NSEntityDescription.entityForName(table, inManagedObjectContext: context)
     
        fetchRequest.entity = entity
        
        if (predicate != nil) {
            fetchRequest.predicate = predicate!
        }
        
        if (sortDescriptors != nil) {
            fetchRequest.sortDescriptors = sortDescriptors!
        }
        
        var error:NSError?
        
        let res:[NSManagedObject] = context.executeFetchRequest(fetchRequest, error: &error) as! [NSManagedObject]
        
        if ((error) != nil) {
            NSLog("Error getDataArray")
        }
        
        return res
    }
    
    static func getDataObject(predicate:NSPredicate, table:String) -> NSManagedObject? {
        return ModelDatabase.getDataArrayWithPredicate(predicate, inTable: table, withSortDescriptors: nil).first
    }
    
    // MARK: Useful Functions
    
    static func addUserMatch(id:Int, name:String, photo:UIImage?, date:NSDate) {
        let context = ModelDatabase.getContext()
        let newUserMatch = NSEntityDescription.insertNewObjectForEntityForName("UserMatch", inManagedObjectContext: context) as! UserMatch
        newUserMatch.name = name
        newUserMatch.id = id
        if (photo != nil) {
            newUserMatch.photo = UIImageJPEGRepresentation(photo, 1.0)
        }
        newUserMatch.date = date
        
        var error:NSError?
        
        context.save(&error)
        
        if ((error) != nil) {
            NSLog("Error addUserMatch")
        }
        
    }

    static func getAllUserMatch() -> [UserMatch] {
        let sortDescriptor:NSSortDescriptor = NSSortDescriptor(key: "date", ascending: false)
        return ModelDatabase.getDataArrayWithPredicate(nil, inTable: "UserMatch", withSortDescriptors: [sortDescriptor]) as! [UserMatch]
    }
    
    static func getUserMatch(id:Int) -> UserMatch? {
        let predicate = NSPredicate(format: "id = \(id)")
        return getDataObject(predicate, table: "UserMatch") as? UserMatch
    }

    static func removeAllUserMatch() {
        let context = ModelDatabase.getContext()
        let userMatches:[UserMatch] = ModelDatabase.getAllUserMatch()
        
        for match in userMatches {
            context.deleteObject(match)
        }
    }

}
