//
//  UserMatch.swift
//  Causons
//
//  Created by Thibault Farnier on 31/05/2015.
//  Copyright (c) 2015 Causons. All rights reserved.
//

import UIKit

class TemporaryUserMatch: NSObject {
    
    let id:Int
    let name:String
    let photo:UIImage?
    
    init(id:Int, name:String, photo:String?) {
        let defaults = NSUserDefaults.standardUserDefaults()
        self.id = id
        self.name = name
        if (photo != nil) {
            self.photo = ImageConverter.fromStringToImage(photo!)
        } else {
            self.photo = nil
        }
    }

}
