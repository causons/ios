//
//  DataSourceViewController.swift
//  Causons
//
//  Created by Thibault Farnier on 22/05/2015.
//  Copyright (c) 2015 Causons. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class MasterViewController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate, MyLogOutDelegate {
    
    // MARK: Properties
    
    var pageViewController = UIPageViewController()
    let mainViewController: MainViewController = UIStoryboard(name: "Main",
        bundle: nil).instantiateViewControllerWithIdentifier("MainViewController") as! MainViewController
    let historyViewController: HistoryViewController = UIStoryboard(name: "Main",
        bundle: nil).instantiateViewControllerWithIdentifier("HistoryViewController") as! HistoryViewController
    let defaults = NSUserDefaults.standardUserDefaults()
    
    // MARK: Outlets
    
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var profilePictureView: FBSDKProfilePictureView!
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var myContainerView: UIView!
    @IBOutlet weak var myActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var barMenu: UIView!

    // MARK: Actions
    
    @IBAction func causonsTouched(sender: UIButton) {
        pageViewController.setViewControllers([mainViewController], direction: .Reverse, animated: true, completion: nil)
        
        UIView.animateWithDuration(0.25, animations: { () -> Void in
            self.barMenu.center.x = self.view.frame.width / 2.0
        })
    }

    @IBAction func historyTouched(sender: UIButton) {
        pageViewController.setViewControllers([historyViewController], direction: .Forward, animated: true, completion: nil)
        
        UIView.animateWithDuration(0.25, animations: { () -> Void in
            self.barMenu.center.x = (5.0 * self.view.frame.width) / 6.0
        })
    }

    @IBAction func settingsTouched(sender: UIButton) {
        mainViewController.stopActivity()
        mainViewController.locationSwitch.setOn(false, animated: false)
    }
    
    // MARK: View Control
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Prepare data for historyView
        historyViewController.update()
        
        myActivityIndicator.hidesWhenStopped = true
        
        if (defaults.boolForKey("isLogged")) {
            let profile = FBSDKProfile.currentProfile()
            welcomeLabel.text = "Hey, \(profile.firstName)!"
        }
        
        
        if (defaults.boolForKey("profilePicFromFB")) {
            self.profilePictureView.alpha = 1.0
            self.profilePic.alpha = 0.0
        } else {
            self.profilePictureView.alpha = 0.0
            self.profilePic.alpha = 1.0
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "didChangeProfile", name: FBSDKProfileDidChangeNotification, object: nil)
    }
    
    override func viewDidAppear(animated: Bool) {
        if (!defaults.boolForKey("isLogged")) {
            // User not logged, show Login View
            super.viewDidAppear(false)
            
            let loginViewController = self.storyboard?.instantiateViewControllerWithIdentifier("LoginViewController") as! UIViewController
            self.presentViewController(loginViewController, animated: false, completion: nil)
            
        } else {
            super.viewDidAppear(true)
            if (defaults.boolForKey("profilePicFromFB")) {
                profilePictureView.pictureMode = FBSDKProfilePictureMode.Square
                UIView.animateWithDuration(
                    1.0,
                    delay: 0,
                    options: UIViewAnimationOptions.BeginFromCurrentState,
                    animations: { () -> Void in
                        self.profilePictureView.alpha = 1.0
                        self.profilePic.alpha = 0.0
                    }, completion: nil)
            } else {
                let docDir: String = (NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true))[0] as! String
                
                let jpgFilePath = "\(docDir)/profilePic.png"
                let data = NSData(contentsOfFile: jpgFilePath)
                self.profilePic.image = UIImage(data: data!)
                UIView.animateWithDuration(
                    1.0,
                    delay: 0,
                    options: UIViewAnimationOptions.BeginFromCurrentState,
                    animations: { () -> Void in
                        self.profilePictureView.alpha = 0.0
                        self.profilePic.alpha = 1.0
                    }, completion: nil)
                
            }
        }
    }
    
    // MARK: Custom Functions

    func didChangeProfile() {
        if (!defaults.boolForKey("isLogged")) {
            // User did just log in
            defaults.setBool(true, forKey: "isLogged")
            let profile = FBSDKProfile.currentProfile()
            welcomeLabel.text = "Hey, \(profile.firstName)!"
            
            defaults.setObject(profile.imagePathForPictureMode(FBSDKProfilePictureMode.Square, size: CGSize(width: 800, height: 800)), forKey: "profilePicPath")
            
            let request = FBSDKGraphRequest(graphPath: "/\(profile.userID)/picture?height=800&width=800&redirect=false", parameters: nil, HTTPMethod: "GET")
            
            var URLString:String = ""
            
            request.startWithCompletionHandler({ (connection, result, error) -> Void in
                if (error == nil) {
                    URLString = result.valueForKey("data")!.valueForKey("url") as! String
                    self.completionProfile(URLString)
                }
            })
            
            
        } else {
            defaults.setBool(false, forKey: "isLogged")
        }
    }
    
    func completionProfile(URLString:String) {
        let profile = FBSDKProfile.currentProfile()
        let URL:NSURL = NSURL(string: URLString)!
        myActivityIndicator.startAnimating()
        self.view.userInteractionEnabled = false
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
            let data:NSData = UIImageJPEGRepresentation(UIImage(data: NSData(contentsOfURL: URL)!), 0.8)
            let photo:String = data.base64EncodedStringWithOptions(nil)
            let token = Server.getToken(profile.userID, firstname: profile.firstName, photo: photo)
            self.defaults.setObject(token, forKey: "userToken")
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.myActivityIndicator.stopAnimating()
                self.view.userInteractionEnabled = true
            })
        })
    }
    
    
    // MARK: Segue
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "contentSegue") {
            
            pageViewController = segue.destinationViewController as! UIPageViewController
            self.pageViewController.delegate = self
            self.pageViewController.dataSource = self
            
            let viewControllers = [mainViewController];
            self.pageViewController.setViewControllers(viewControllers, direction: .Forward, animated: true, completion: nil)
            
            
            // Change the size of page view controller
            self.pageViewController.view.frame = CGRectMake(0,0, self.myContainerView.frame.size.width, self.myContainerView.frame.size.height)
            
            self.pageViewController.didMoveToParentViewController(self)
        } else if (segue.identifier == "presentSettings") {
            let navigationController = segue.destinationViewController as! UINavigationController
            let settingsViewController = navigationController.topViewController as! SettingsTableViewController
            settingsViewController.logOutDelegate = self
        }
    }

    
    // MARK: LogOutDelegate
    
    func didRequestLogOut() {
        let loginManager = FBSDKLoginManager()
        loginManager.logOut()
        defaults.setObject(nil, forKey: "userToken")
        defaults.setBool(true, forKey: "isFirstUse")
        pageViewController.setViewControllers([mainViewController], direction: .Reverse, animated: true, completion: nil)
        let loginViewController = self.storyboard?.instantiateViewControllerWithIdentifier("LoginViewController") as! UIViewController
        self.presentViewController(loginViewController, animated: true, completion: nil)
    }
    
    // MARK: PageView Protocol Functions
    
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [AnyObject], transitionCompleted completed: Bool) {
        if (completed) {
            if (previousViewControllers.first is MainViewController &&
                !(pageViewController.presentedViewController is MainViewController)) {
                UIView.animateWithDuration(0.25 , delay: 0,
                    options: UIViewAnimationOptions.BeginFromCurrentState,
                    animations: { () -> Void in
                        self.barMenu.center.x = (5.0 * self.view.frame.width) / 6.0
                    },
                    completion: nil)
            } else if (previousViewControllers.first is HistoryViewController &&
                !(pageViewController.presentedViewController is HistoryViewController)) {
                UIView.animateWithDuration(0.25 , delay: 0,
                    options: UIViewAnimationOptions.BeginFromCurrentState,
                    animations: { () -> Void in
                        self.barMenu.center.x = self.view.frame.width / 2.0
                    },
                    completion: nil)
            }
        }
    }
    
    
    // MARK: DataSource Protocol Functions
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 2
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        if (viewController is MainViewController) {
            return historyViewController
        } else {
            return nil
        }
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        if (viewController is HistoryViewController) {
            return mainViewController
        } else {
            return nil
        }
    }
    
}
