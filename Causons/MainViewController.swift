//
//  MainViewController.swift
//  Causons
//
//  Created by Thibault Farnier on 21/05/2015.
//  Copyright (c) 2015 Causons. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import CoreLocation

@objc protocol SearchPersonDelegate {
    func didFindSomeone()
}

class MainViewController: UIViewController, CLLocationManagerDelegate {
    
    // MARK: Properties
    
    var locationManager: CLLocationManager?
    var myTimer:NSTimer = NSTimer()
    var locationTimer:NSTimer = NSTimer()
    var location:CLLocation = CLLocation()
    var justLaunched:Bool = true
    var stopRunning:Bool = false
    var maySendLocation:Bool = false
    var userMatch:TemporaryUserMatch? = nil
    let defaults = NSUserDefaults.standardUserDefaults()
    
    // MARK: Outlets
    
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var locationSwitch: UISwitch!
    @IBOutlet weak var myActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var foundPicView: UIImageView!
    @IBOutlet weak var prenomTextField: UITextField!
    
    // MARK: View Functions
    
    override func viewDidLoad() {
        self.locationManager = CLLocationManager()
        self.myActivityIndicator.hidesWhenStopped = true
        self.myActivityIndicator.stopAnimating()
        self.foundPicView.alpha = 0.0
        self.prenomTextField.alpha = 0.0
        locationSwitch.tintColor = UIColor.blackColor()
        defaults.setBool(true, forKey: "justLaunched")
        locationLabel.text = "Switchez pour causer!"
        if CLLocationManager.locationServicesEnabled() {
            locationManager?.requestWhenInUseAuthorization()
            locationManager?.delegate = self
            locationManager?.desiredAccuracy = kCLLocationAccuracyBest
            //locationManager?.startUpdatingLocation()
        } else {
            locationLabel.text = "Localisation non autorisée :("
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "textEdited", name: UITextFieldTextDidChangeNotification, object: nil)
    }
    
    // MARK: Location
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        location = locations.last as! CLLocation
        // prevent to send several locations at the same time
    }
    
    // MARK: Update Location
    
    func sendLocationThreaded() {
        if (maySendLocation) {
            NSThread.detachNewThreadSelector("sendLocation", toTarget: self, withObject: nil)
        }
    }
    
    func sendLocation() {
        //NSLog("Sending Location")
        Server.sendLocation(location.coordinate.longitude, latitude: location.coordinate.latitude)
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.locationTimer.invalidate()
            self.locationTimer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: "sendLocationThreaded", userInfo: nil, repeats: false)
        })
    }
    
    // MARK: Search
    
    func findSomeoneThreaded() {
        NSThread.detachNewThreadSelector("findSomeone", toTarget: self, withObject: nil)
    }
    
    func findSomeone() {
        //NSLog("entered findSomeone")
        // At start coordinates are always at (0,0). Ignore it.
        if (location.coordinate.longitude == 0 && location.coordinate.latitude == 0) {
            println("0,0")
            locationLabel.text = "Je cherche..."
            // Anything concerning nstimers must be performed on main queue
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.myTimer.invalidate()
                self.myTimer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: "findSomeoneThreaded", userInfo: nil, repeats: false)
            })
            return
        }
        
        userMatch = Server.getUserAround(location.coordinate.longitude, latitude: location.coordinate.latitude)
        
        // limits thread-linked risks since previous instruction is long
        if (stopRunning) {
            return
        }
        
        if (userMatch != nil) {
            let previousMatches = ModelDatabase.getAllUserMatch()
            if (ModelDatabase.getUserMatch(userMatch!.id) != nil) {
                defaults.setInteger(-1, forKey: "lastFoundId")
                locationLabel.text = "Je cherche..."
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.myTimer.invalidate()
                    self.myTimer = NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector: "findSomeoneThreaded", userInfo: nil, repeats: false)
                })
                return
            }
            defaults.setInteger(userMatch!.id, forKey: "lastFoundId")
            defaults.setObject(userMatch!.name, forKey: "lastFoundName")
            defaults.setObject(ImageConverter.fromImageToString(userMatch!.photo!), forKey: "lastFoundPhoto")
            
            if (defaults.boolForKey("isFirstUse")) {
                presentInstructions()
                defaults.setBool(false, forKey: "isFirstUse")
            }
            
            locationLabel.text = "Causez!"
            foundPicView.image = userMatch!.photo
            myActivityIndicator.stopAnimating()
            showPic()

            // Anything concerning nstimers must be performed on main queue
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.myTimer.invalidate()
                self.myTimer = NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector: "isStillAroundThreaded", userInfo: nil, repeats: false)
            })
        } else {
            defaults.setInteger(-1, forKey: "lastFoundId")
            locationLabel.text = "Je cherche..."
            
            // Anything concerning nstimers must be performed on main queue
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.myTimer.invalidate()
                self.myTimer = NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: "findSomeoneThreaded", userInfo: nil, repeats: false)
            })
        }
    }
    
    func presentInstructions() {
        let alertController = UIAlertController(title: "Vous avez trouvé quelqu'un avec qui causer !", message: "Allez lui parler, et entrez son prénom pour valider la discussion.", preferredStyle: UIAlertControllerStyle.Alert)
        let action = UIAlertAction(title: "Ok, j'ai compris !", style: UIAlertActionStyle.Default, handler: nil)
        alertController.addAction(action)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func isStillAroundThreaded() {
        NSThread.detachNewThreadSelector("isStillAround", toTarget: self, withObject: nil)
    }
    
    func isStillAround() {
        //NSLog("entered isStillAround")
        let id = defaults.integerForKey("lastFoundId")
        
        if (id == -1) {
            justLaunched = false
            hidePic()
            findSomeoneThreaded()
            return
        }
        
        let isStillAround:Bool = Server.isStillAround(id, longitude: location.coordinate.longitude, latitude: location.coordinate.latitude)
        
        // limits thread-linked risks
        if (stopRunning) {
            return
        }
        
        if (isStillAround) {
            if (justLaunched == true) {
                let name = defaults.objectForKey("lastFoundName") as! String
                let photo = defaults.objectForKey("lastFoundPhoto") as! String
                userMatch = TemporaryUserMatch(id: id, name: name, photo: photo)
                locationLabel.text = "Causez!"
                foundPicView.image = userMatch!.photo
                myActivityIndicator.stopAnimating()
                showPic()
                justLaunched = false                
            }
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.myTimer.invalidate()
                self.myTimer = NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector: "isStillAroundThreaded", userInfo: nil, repeats: false)
            })
        } else {
            justLaunched = false
            hidePic()
            myActivityIndicator.startAnimating()
            locationLabel.text = "Je cherche..."
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.myTimer.invalidate()
                self.myTimer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: "findSomeoneThreaded", userInfo: nil, repeats: false)
            })
        }
    }

    // MARK: Custom Functions
    
    func textEdited() {
        if (prenomTextField.text.caseInsensitiveCompare(userMatch!.name) == NSComparisonResult.OrderedSame) {
            self.view.endEditing(true)
            ModelDatabase.addUserMatch(userMatch!.id, name: userMatch!.name, photo: userMatch!.photo, date:NSDate())
            defaults.setInteger(-1, forKey: "lastFoundId")
            prenomTextField.text = ""
            startActivity()
            let alertController = UIAlertController(title: "Discussion validée !", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
            let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                alertController.dismissViewControllerAnimated(true, completion: nil)
            })
            NSThread.detachNewThreadSelector("sendMeeting", toTarget: self, withObject: nil)
            alertController.addAction(action)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    func sendMeeting() {
        Server.sendMeeting(userMatch!.id)
    }
    
    func startActivity() {
        self.locationLabel.text = "Je cherche..."
        justLaunched = true
        stopRunning = false
        maySendLocation = true
        locationManager?.startUpdatingLocation()
        locationTimer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: "sendLocationThreaded", userInfo: nil, repeats: false)
        myTimer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: "isStillAroundThreaded", userInfo: nil, repeats: false)
        myActivityIndicator.startAnimating()
        
        UIView.animateWithDuration(
            1.0,
            delay: 0.10,
            options: UIViewAnimationOptions.BeginFromCurrentState,
            animations: { () -> Void in
                self.locationSwitch.transform = CGAffineTransformMake(1, 0, 0, 1, -self.view.frame.size.width/2 + 42, self.view.frame.size.height/2 - 70)
                self.locationLabel.transform = CGAffineTransformMake(1, 0, 0, 1, self.view.frame.size.width/8, self.view.frame.size.height - self.locationLabel.center.y - 32)
            }, completion: nil)
    }
    
    func stopActivity() {
        myTimer.invalidate()
        stopRunning = true
        maySendLocation = false
        locationManager?.stopUpdatingLocation()
        myActivityIndicator.stopAnimating()
        UIView.animateWithDuration(
            1.0,
            delay: 0.10,
            options: UIViewAnimationOptions.BeginFromCurrentState,
            animations: { () -> Void in
                self.locationSwitch.transform = CGAffineTransformMake(1, 0, 0, 1, 0, 0)
                self.locationLabel.transform = CGAffineTransformMake(1, 0, 0, 1, 0, 0)
            }, completion: nil)
        hidePic()
        locationLabel.text = "Switchez pour causer!"
    }
    
    func hidePic() {
        UIView.animateWithDuration(
            1.0,
            delay: 0,
            options: UIViewAnimationOptions.BeginFromCurrentState,
            animations: { () -> Void in
                self.foundPicView.alpha = 0.0
                self.prenomTextField.alpha = 0.0
            }, completion: nil)
    }
    
    func showPic() {
        UIView.animateWithDuration(
            1.0,
            delay: 0,
            options: UIViewAnimationOptions.BeginFromCurrentState,
            animations: { () -> Void in
                self.foundPicView.alpha = 1.0
                self.prenomTextField.alpha = 1.0
            }, completion: nil)
    }
    
    // MARK: Actions
    
    @IBAction func didTap(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    @IBAction func locationSwitchChanged(sender: UISwitch) {
        if (sender.on) {
            startActivity()
        } else {
            stopActivity()
        }
    }
    
}
