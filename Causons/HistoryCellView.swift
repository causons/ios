//
//  HistoryViewCell.swift
//  Causons
//
//  Created by Thibault Farnier on 28/05/2015.
//  Copyright (c) 2015 Causons. All rights reserved.
//

import UIKit

class HistoryCellView: UITableViewCell {

    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var firstnameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
