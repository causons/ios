//
//  HistoryViewController.swift
//  Causons
//
//  Created by Thibault Farnier on 28/05/2015.
//  Copyright (c) 2015 Causons. All rights reserved.
//

import UIKit

class HistoryViewController: UITableViewController {
    
    // MARK: - Properties
    
    var userMatches:[UserMatch]?
    
    // MARK: - View Management

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerNib(UINib(nibName: "HistoryCellView", bundle: nil), forCellReuseIdentifier: "HistoryCellView")
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        NSThread.detachNewThreadSelector("update", toTarget: self, withObject: nil)
    }
    
    func update() {
        userMatches = ModelDatabase.getAllUserMatch()
        self.tableView.reloadData()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (userMatches!.count > 0) {
            // +1 pour la case permettant d'effacer l'historique
            return userMatches!.count + 1
        } else {
            return 0
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if (indexPath.row < userMatches!.count) {
            
            var cell = tableView.dequeueReusableCellWithIdentifier("HistoryCellView", forIndexPath: indexPath) as? HistoryCellView
            
            if ((cell) == nil) {
                // On charge la cellule personnalisée
                let nib = NSBundle.mainBundle().loadNibNamed("HistoryCellView", owner: self, options: nil)
                cell = nib[0] as? HistoryCellView
            }
            
            if (userMatches?.count != 0) {
                let userMatch = userMatches![indexPath.row]
                cell?.firstnameLabel.text = userMatch.name
                let format = NSDateFormatter()
                format.dateFormat = "dd/MM/yyyy à HH:mm"
                cell?.dateLabel.text = format.stringFromDate(userMatch.date)
                cell?.picture.image = UIImage(data: userMatch.photo)
            } else {
                // On ne devrait jamais arriver là
                cell?.firstnameLabel.text = "Hi!"
            }
            
            return cell!
            
        } else {
            var cell = tableView.dequeueReusableCellWithIdentifier("ClearCellView", forIndexPath: indexPath) as? UITableViewCell
            
            return cell!
        }
        
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 88
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (indexPath.row == userMatches!.count) {
            // On est à la dernière case ie la case pour effacer l'historique
            ModelDatabase.removeAllUserMatch()
            update()
        }
    }
}
