//
//  ImageConverter.swift
//  Causons
//
//  Created by Thibault Farnier on 03/06/2015.
//  Copyright (c) 2015 Causons. All rights reserved.
//

import UIKit

class ImageConverter : NSObject {
    
    static func fromStringToImage(photoString:String) -> UIImage {
        return UIImage(data: NSData(base64EncodedString: photoString, options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)!)!
    }
    
    static func fromImageToString(image:UIImage) -> String {
        let data = NSData(data: UIImageJPEGRepresentation(image, 0.8))
        return data.base64EncodedStringWithOptions(nil)
    }
   
}
