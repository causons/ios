//
//  LoginViewController.swift
//  Causons
//
//  Created by Thibault Farnier on 22/05/2015.
//  Copyright (c) 2015 Causons. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class LoginViewController: UIViewController, FBSDKLoginButtonDelegate {

    // MARK: Outlets
    @IBOutlet weak var loginButton: FBSDKLoginButton!
    
    // MARK: View control
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginButton.delegate = self
        FBSDKProfile.enableUpdatesOnAccessTokenChange(true)
        /*let loginButton:FBSDKLoginButton = FBSDKLoginButton()
        loginButton.center = self.view.center
        self.view.addSubview(loginButton)*/
    }
    
    // MARK: FB Login handling
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        if (!result.isCancelled) {
            let defaults = NSUserDefaults.standardUserDefaults()
            //defaults.setBool(true, forKey: "isLogged")
            defaults.setBool(true, forKey: "profilePicFromFB")
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setBool(false, forKey: "isLogged")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
