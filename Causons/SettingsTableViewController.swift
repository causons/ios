//
//  SettingsTableViewController.swift
//  Causons
//
//  Created by Thibault Farnier on 26/05/2015.
//  Copyright (c) 2015 Causons. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import MessageUI

@objc protocol MyLogOutDelegate{
    func didRequestLogOut()
}

class SettingsTableViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, MFMailComposeViewControllerDelegate {
    
    // MARK: - Properties
    
    var logOutDelegate: MyLogOutDelegate?
    
    var imagePicker:UIImagePickerController!
    var mailController:MFMailComposeViewController?
    let defaults = NSUserDefaults.standardUserDefaults()
    
    // MARK: - Outlets
    
    @IBOutlet weak var myActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var backButtonItem: UIBarButtonItem!
    
    // MARK: - Actions
    
    @IBAction func retourButtonTouched(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    // MARK: - View Management
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.myActivityIndicator.hidesWhenStopped = true
        self.myActivityIndicator.stopAnimating()
        
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Futura-CondensedMedium", size: 20)!]
        
        self.backButtonItem.setTitleTextAttributes([ NSFontAttributeName: UIFont(name: "Futura", size: 16)!], forState: .Normal)
    }
    
    // MARK: - Custom Functions
    
    func launchCamera() {
        imagePicker!.sourceType = UIImagePickerControllerSourceType.Camera
        self.presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    func showGallery() {
        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary)) {
            imagePicker!.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
    }
    
    func prepareChangeProfilePic() {
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        
        var alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        var FBAction = UIAlertAction(title: "Photo de profil Facebook", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            self.defaults.setBool(true, forKey: "profilePicFromFB")
            NSThread.detachNewThreadSelector("updateProfilePic:", toTarget: Server.self, withObject: nil)
        }
        var cameraAction = UIAlertAction(title: "Appareil Photo", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            self.launchCamera()
        }
        var galleryAction = UIAlertAction(title: "Galerie", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            self.showGallery()
        }
        var cancelAction = UIAlertAction(title: "Annuler", style: UIAlertActionStyle.Cancel, handler: nil)
        
        
        
        alertController.addAction(FBAction)
        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)) {
            alertController.addAction(cameraAction)
        }
        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary)) {
            alertController.addAction(galleryAction)
        }
        alertController.addAction(cancelAction)
        
        
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func presentInstructions() {
        let alertController = UIAlertController(title: "Instructions", message: "Lorsque la photo d'une personne s'affiche, vous pouvez aller lui parler. Une fois que c'est fait, entrez son prénom pour valider la discussion, l'enregistrer dans l'historique et trouver une autre personne avec qui discuter !", preferredStyle: UIAlertControllerStyle.Alert)
        let action = UIAlertAction(title: "Ok, j'ai compris !", style: UIAlertActionStyle.Default, handler: nil)
        alertController.addAction(action)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func sendEmail() {
        if (MFMailComposeViewController.canSendMail()) {
            mailController = MFMailComposeViewController()
            mailController!.mailComposeDelegate = self
            mailController!.setToRecipients(["causonsteam@causons.fr"])
            mailController!.setSubject("Support Causons")
            self.presentViewController(mailController!, animated: true, completion: nil)
        } else {
            let sendMailErrorAlert = UIAlertView(title: "Erreur Mail", message: "Votre appareil ne peut pas envoyer de mail.", delegate: self, cancelButtonTitle: "OK")
            sendMailErrorAlert.show()
        }
    }
    
    func prepareLogOut() {
        let profile = FBSDKProfile.currentProfile()
        var alertController = UIAlertController(title: "Connecté en tant que \(profile.firstName) \(profile.lastName)", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        var confirmLogOut = UIAlertAction(title: "Se déconnecter", style: UIAlertActionStyle.Destructive) {
            UIAlertAction in
            self.dismissViewControllerAnimated(true, completion: { () -> Void in
                self.logOutDelegate?.didRequestLogOut()
            })
        }
        var cancelAction = UIAlertAction(title: "Annuler", style: UIAlertActionStyle.Cancel, handler: nil)
        alertController.addAction(confirmLogOut)
        alertController.addAction(cancelAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerView Delegate Functions
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        let docDir: String = (NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true))[0] as! String
        
        let jpgFilePath = "\(docDir)/profilePic.png"
        let data = NSData(data: UIImageJPEGRepresentation(image, 1.0))
        data.writeToFile(jpgFilePath, atomically: true)
        
        defaults.setBool(false, forKey: "profilePicFromFB")
        /*[NSThread detachNewThreadSelector:@selector(loadImageInBackground)
        toTarget:self withObject:nil];*/
        //NSThread(target: Server.classForCoder(), selector: "updateProfilePic:", object: image)
        NSThread.detachNewThreadSelector("updateProfilePic:", toTarget: Server.self, withObject: image)
        picker.dismissViewControllerAnimated(true, completion:nil)
    }
    
    // MARK: - Mail Delegate
    
    func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
        mailController?.dismissViewControllerAnimated(true, completion: nil)
    }

    // MARK: - Table View Data Source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 4
    }

    
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! UITableViewCell

        // Configure the cell...
        
        return cell
    }
    */

    
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return false
    }

    
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return false
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch(indexPath.row) {
        case 0:
            // Change profile pic
            self.prepareChangeProfilePic()
        case 1:
            self.presentInstructions()
        case 2:
            self.sendEmail()
        case 3:
            self.prepareLogOut()
        default:
            let a = 0
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.

    }
    */

}
