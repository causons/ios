//
//  Server.swift
//  Causons
//
//  Created by Thibault Farnier on 29/05/2015.
//  Copyright (c) 2015 Causons. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class Server: NSObject {
    
    // MARK: - Generic Function
    
    ///     This method allows to send generic POST requests
    /// :JSONString:    The JSON Object, in a String
    /// :URL:           The URL to send the JSON String
    /// :headerDictionnary: A dictionnary containing <headerField, content>, to fill the header in the request
    /// :answerToRead:  An array with the Strings of the answers to read. Answers are given in an array the same order as in answerToRead
    
    static func POST(JSONString:String, URL:NSURL, headerDictionnary:Dictionary<String,String>, answerToRead:[String]?) -> [String]? {
        /* Inspired from https://dipinkrishna.com/blog/2014/07/login-signup-screen-tutorial-xcode-6-swift-ios-8-json/ */
        
        var postData:NSData = JSONString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!
        
        var request = NSMutableURLRequest(URL: URL)
        request.HTTPMethod = "POST"
        request.HTTPBody = postData
        for (headerField, content) in headerDictionnary {
            request.setValue(content, forHTTPHeaderField: headerField)
        }
        
        var responseError: NSError?
        var response: NSURLResponse?
        
        var urlData: NSData? = NSURLConnection.sendSynchronousRequest(request, returningResponse:&response, error:&responseError)
        
        if (urlData != nil) {
            let res = response as! NSHTTPURLResponse!;
            
            //NSLog("Response code: %ld", res.statusCode);
            
            if (res.statusCode >= 200 && res.statusCode < 300) {
                if ((answerToRead) != nil) {
                    var responseData:NSString  = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
                    
                    //NSLog("Response ==> %@", responseData);
                    
                    var error: NSError?
                    
                    let jsonData:NSDictionary = NSJSONSerialization.JSONObjectWithData(urlData!, options:NSJSONReadingOptions.MutableContainers , error: &error) as! NSDictionary
                    
                    var answers:[String] = []
                    
                    if (answerToRead != nil) {
                        if (jsonData.count == 0) {
                            return nil
                        }
                        
                        let answersToRead = answerToRead!
                        
                        for answer in answersToRead {
                            if (jsonData.valueForKey(answer) is NSNumber) {
                                answers.append("\(jsonData.valueForKey(answer) as! Int)")
                            } else {
                                answers.append(jsonData.valueForKey(answer) as! NSString as String)
                            }
                        }
                    }

                    return answers
                }
            } else {
                println("Error: Status code = \(res.statusCode)")
                println("Request URL = \(URL)")
                println("JSON Object = \(JSONString)")
                println("HTTP Headers = \(headerDictionnary)")
            }
        } else {
            NSLog("Error: no URL data")
        }
        
        return nil
    }
    
    // MARK: - Useful Functions

    static func sendMeeting(passiveId:Int) {
        let defaults = NSUserDefaults.standardUserDefaults()
        let token = defaults.objectForKey("userToken") as! String
        
        let post = "{\"meeting\" : {\"passive_id\": \"\(passiveId)\"}}"
        
        let URL:NSURL = NSURL(string: "http://causons.herokuapp.com/api/meetings")!
        
        let headerDictionnary: Dictionary<String, String> = ["Content-Type":"application/json", "Authorization":"token \(token)"]

        POST(post, URL:URL, headerDictionnary: headerDictionnary, answerToRead:nil)
    }

    static func isStillAround(id:Int, longitude:Double, latitude:Double) -> Bool {
        let defaults = NSUserDefaults.standardUserDefaults()
        let token = defaults.objectForKey("userToken") as! String
        
        let post = "{\"user\" : {\"id\": \"\(id)\" ,\"latitude\": \"\(latitude)\", \"longitude\": \"\(longitude)\"}}"
        
        let URL:NSURL = NSURL(string: "http://causons.herokuapp.com/api/users/isStillAround")!
        
        let headerDictionnary: Dictionary<String, String> = ["Content-Type":"application/json", "Authorization":"token \(token)"]
        
        var res:[String]? = POST(post, URL: URL, headerDictionnary: headerDictionnary, answerToRead: ["is_still_around"])
        if (res != nil) {
            var resu = res!
            return (resu[0].toInt()! == 1) ? true : false
        } else {
            return true
        }
    }
    
    static func getUserAround(longitude:Double, latitude:Double) -> TemporaryUserMatch? {
        let defaults = NSUserDefaults.standardUserDefaults()
        let token = defaults.objectForKey("userToken") as! String
        
        let post = "{\"user\" : {\"latitude\": \"\(latitude)\", \"longitude\": \"\(longitude)\"}}"
        
        let URL:NSURL = NSURL(string: "http://causons.herokuapp.com/api/users/getUserAround")!
        
        let headerDictionnary: Dictionary<String, String> = ["Content-Type":"application/json", "Authorization":"token \(token)"]
        
        var res:[String]? = POST(post, URL: URL, headerDictionnary: headerDictionnary, answerToRead: ["id","name","photo"])
        if (res != nil) {
            var resu = res!
            return TemporaryUserMatch(id: resu[0].toInt()!, name: resu[1], photo: resu[2])
        } else {
            return nil
        }
    }
    
    static func updateProfilePic(profilePic:UIImage?) {
        let defaults = NSUserDefaults.standardUserDefaults()
        let token = defaults.objectForKey("userToken") as! String
        
        if (defaults.boolForKey("profilePicFromFB")) {
            let profile = FBSDKProfile.currentProfile()
            
            let path:String = profile.imagePathForPictureMode(FBSDKProfilePictureMode.Square, size: CGSize(width: 800,height: 800))
            
            let request = FBSDKGraphRequest(graphPath: path, parameters: nil, HTTPMethod: "POST")
            
            var URLString:String = ""
            
            let imgURLString = "http://graph.facebook.com/\(profile.userID)/picture?width=800&height=800"
            let imgURL = NSURL(string: imgURLString)
            let imgData = NSData(contentsOfURL: imgURL!)
            let image = UIImage(data: imgData!)
            let URL:NSURL = NSURL(string: URLString)!
            let photo:String = ImageConverter.fromImageToString(image!)
            let post = "{\"user\" : {\"photo\": \"\(photo)\"}}"
            let postURL:NSURL = NSURL(string: "http://causons.herokuapp.com/api/users/updatePhoto")!
            let headerDictionnary: Dictionary<String, String> = ["Content-Type":"application/json", "Authorization":"token \(token)"]
            POST(post, URL: postURL, headerDictionnary: headerDictionnary, answerToRead: nil)
            
        } else {
            
            let data:NSData = UIImageJPEGRepresentation(profilePic, 0.75)
            let photo:String = data.base64EncodedStringWithOptions(nil)
            
            let post = "{\"user\" : {\"photo\": \"\(photo)\"}}"
            
            let postURL:NSURL = NSURL(string: "http://causons.herokuapp.com/api/users/updatePhoto")!
            
            let headerDictionnary: Dictionary<String, String> = ["Content-Type":"application/json", "Authorization":"\(token)"]
            
            POST(post, URL: postURL, headerDictionnary: headerDictionnary, answerToRead: nil)
        }
    }
    
    static func setUserVisible(isVisible:Bool) {
        let defaults = NSUserDefaults.standardUserDefaults()
        let token = defaults.objectForKey("userToken") as! String
        
        let post = "{\"user\" : {\"visible\": \"\(isVisible)\"}}"
        
        let URL:NSURL = NSURL(string: "http://causons.herokuapp.com/api/users/updateVisible")!
        
        let headerDictionnary: Dictionary<String, String> = ["Content-Type":"application/json", "Authorization":"\(token)"]
        
        POST(post, URL: URL, headerDictionnary: headerDictionnary, answerToRead: nil)
        
    }
    
    static func sendLocation(longitude:Double, latitude:Double) {
        let defaults = NSUserDefaults.standardUserDefaults()
        let token = defaults.objectForKey("userToken") as! String

        let post = "{\"user\" : {\"latitude\": \"\(latitude)\", \"longitude\": \"\(longitude)\"}}"
        
        let URL:NSURL = NSURL(string: "http://causons.herokuapp.com/api/users/updatePosition")!

        let headerDictionnary: Dictionary<String, String> = ["Content-Type":"application/json", "Authorization":"\(token)"]
        
        POST(post, URL: URL, headerDictionnary: headerDictionnary, answerToRead: nil)
    }

    static func getToken(userID:String, firstname:String, photo:String) -> String {

        let post = "{\"auth\": {\"email\" : \"\(userID)\", \"password\" : \"\(userID)\", \"name\" : \"\(firstname)\", \"photo\" : \"\(photo)\"}} "
        
        let URL:NSURL = NSURL(string: "http://causons.herokuapp.com/public/facebookAuthenticate")!
        let answerToRead = "token"
        let headerDictionnary: Dictionary<String, String> = ["Content-Type":"application/json"]
        
        return (POST(post, URL: URL, headerDictionnary: headerDictionnary, answerToRead: [answerToRead])!)[0]
    }
   
}
