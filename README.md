# Causons! #

Causons! (="Let's talk" in French) is an app that allows you to find people nearby you, willing to have a discussion with someone they don't know!

### How does it work? ###

* Turn on the search toggle.
* A picture of someone nearby you will appear!
* To confirm you've met and talked to this person, enter his/her name in the bar. The next one will appear soon!

### Features ###

* Match according to location
* Facebook Connection
* Ability to use own photo or Facebook's
* History of meetings

### Coder of the iOS version ###

* Thibault Farnier

### An original idea by ###

* Nicolas Brengard
* Thibault Farnier
* Jérôme Lapostolet
* Rémy Rey