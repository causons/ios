//
//  UserMatch.swift
//  Causons
//
//  Created by Thibault Farnier on 04/06/2015.
//  Copyright (c) 2015 Causons. All rights reserved.
//

import Foundation
import CoreData

class UserMatch: NSManagedObject {

    @NSManaged var id: NSNumber
    @NSManaged var name: String
    @NSManaged var photo: NSData
    @NSManaged var date: NSDate

}
